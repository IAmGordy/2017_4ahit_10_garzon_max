﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Briefkasten
    {
        public void briefEinwerfen(Dokument dokument)
        {
            Console.WriteLine(dokument.name +" ist eingeworfen.");
        }
    }
}
