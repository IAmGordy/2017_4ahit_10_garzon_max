﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Computer
    {
        public void an()
        {
            Console.WriteLine("Computer angeschalten");
        }

        public void aus()
        {
            Console.WriteLine("Computer ausgeschalten");
        }

        public void drucke(Dokument dokument)
        {
            Console.WriteLine(dokument.name + " wurde gedruckt");
        }

        public Textverarbeitungsprogramm getTextverarbeitungsprogramm()
        {
            return new Textverarbeitungsprogramm();
        }
    }
}
