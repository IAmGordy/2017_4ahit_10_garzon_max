﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Stift
    {
        public void unterschreiben(Dokument dokument)
        {
            Console.WriteLine(dokument.name + " wurde unterschrieben.");
        }
    }
}
