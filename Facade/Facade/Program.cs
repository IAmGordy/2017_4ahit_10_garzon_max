﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            Sekretär sekretär = new Sekretär();
            sekretär.schreibenVersenden("Ja moin");
            Console.ReadKey();
        }
    }
}
