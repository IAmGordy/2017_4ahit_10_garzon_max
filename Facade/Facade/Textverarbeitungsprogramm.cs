﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Textverarbeitungsprogramm
    {
        public Dokument getDokument(string name)
        {
            Dokument dokument = new Dokument();
            dokument.name = name;
            return dokument;
        }
        public void oeffnen()
        {
            Console.WriteLine("Programm geöffnet");
        }

        public void schließen()
        {
            Console.WriteLine("Programm geschlossen");
        }
    }
}
