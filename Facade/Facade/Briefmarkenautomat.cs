﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Briefmarkenautomat
    {
        private int geld = 1000;
        public void briefmarkeBezahlen(Dokument dokument, int wert)
        {
            if(geld>wert)
            Console.WriteLine(dokument.name + " hat nun eine Briefmarke");
            geld -= wert;
        }
    }
}
