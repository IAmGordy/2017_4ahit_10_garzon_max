﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Drucker
    {
        public void an()
        {
            Console.WriteLine("Computer angeschalten");
        }

        public void aus()
        {
            Console.WriteLine("Computer ausgeschalten");
        }
        
        public void konfigurieren()
        {
            Console.WriteLine("Computer ist konfiguriert");
        }

        public void papierNachfuellen()
        {
            Console.WriteLine("Papier wurde nachgefüllt");
        }
    }
}
