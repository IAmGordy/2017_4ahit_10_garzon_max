﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Stempel
    {
        public void stempeln(Dokument dokument)
        {
            Console.WriteLine(dokument.name +" wurde gestempelt.");
        }
    }
}
