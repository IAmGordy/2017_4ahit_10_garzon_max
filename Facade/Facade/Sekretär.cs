﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class Sekretär
    {
        public void schreibenVersenden(String text)
        {
            //Gegeben: Alle benötigten Klassen sind korrekt instanziiert, initialisiert und bekannt
            Computer computer = new Computer();
            Drucker drucker = new Drucker();
            Textverarbeitungsprogramm textverarbeitungsprog = computer.getTextverarbeitungsprogramm();
            Stift stift = new Stift();
            Stempel stempel = new Stempel();
            Briefmarkenautomat briefmarkenautomat = new Briefmarkenautomat();
            Briefkasten briefkasten = new Briefkasten();
            Dokument dokument = textverarbeitungsprog.getDokument(text);
            dokument.name = "Textfile.txt";

            computer.an();            
       
            textverarbeitungsprog.oeffnen();           

            drucker.an();

            drucker.konfigurieren();

            drucker.papierNachfuellen();

            computer.drucke(dokument);

            drucker.aus();

            computer.aus();

            stift.unterschreiben(dokument);

            stempel.stempeln(dokument);

            briefmarkenautomat.briefmarkeBezahlen(dokument, 2);

            briefkasten.briefEinwerfen(dokument);
        }
    }
}
