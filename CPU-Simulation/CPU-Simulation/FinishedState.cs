﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class FinishedState: IState
    {
        public IState Execute(MyThread myth)
        {
            Console.WriteLine(myth.name + " Process finished");
            
            return new FinishedState();
        }
    }
}
