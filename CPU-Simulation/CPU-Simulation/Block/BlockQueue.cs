﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class BlockQueue : Queue
    {
        public static BlockQueue blockedqueue;

        public static BlockQueue GetInstance()
        {
            return (blockedqueue == null) ? blockedqueue = new BlockQueue() : blockedqueue;
        }
    }
}
