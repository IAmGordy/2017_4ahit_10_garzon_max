﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CPU_Simulation
{
    class BlockedState : IState
    {
        
        
        public IState Execute(MyThread myth)
        {
            
            Console.WriteLine(myth.name + ".OldState: " + myth.state.ToString());
            
            Console.WriteLine(myth.name + ".NewState: Ready");
            ReadyQueue.GetInstance().Enqueue(myth);
            return new ReadyState();
        }
    }
}
