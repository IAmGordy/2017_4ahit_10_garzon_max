﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    
    class BlockQueueConsumer
    {
        BlockQueue blockedqueue = BlockQueue.GetInstance();
        public void Consume()
        {
            while(true)
            {
                if(blockedqueue.Count > 0)
                {
                    MyThread myth = (MyThread)blockedqueue.Dequeue();
                    myth.changeState();
                }
            }
            
        }
    }
}
