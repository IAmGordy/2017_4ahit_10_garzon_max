﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    

    class RunningQueueConsumer
    {

        RunningQueue runningQueue = RunningQueue.GetInstance();
        public void Consume()
        {
            while (true)
            {
                if(runningQueue.Count > 0)
                {
                    MyThread myt = (MyThread)runningQueue.Dequeue();
                    myt.changeState();
                }
                
            }
            
        }
    }
}
