﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class RunningQueue : Queue
    {
        public static RunningQueue runningqueue;

        public static RunningQueue GetInstance()
        {
            return (runningqueue == null) ? runningqueue = new RunningQueue() : runningqueue;
        }
    }
}
