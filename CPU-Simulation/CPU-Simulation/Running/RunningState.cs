﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class RunningState : IState
    {
        static Random r = new Random();
        int number;
        public IState Execute(MyThread myth)
        {
            Console.WriteLine(myth.name + ".OldState: " + myth.state.ToString());
            number = r.Next(1, 100);
            if (number < 50)
            {
                Console.WriteLine(myth.name + ".NewState: Ready");
                ReadyQueue.GetInstance().Enqueue(myth);
                return new ReadyState();
            }
                
            if (number >= 50 && number < 80)
            {
                Console.WriteLine(myth.name + ".NewState: Finished");

                return new FinishedState();
            }
                
            else
            {
                Console.WriteLine(myth.name + ".NewState: Blocked");
                BlockQueue.GetInstance().Enqueue(myth);
                return new BlockedState();
            }
        }
    }
}
