﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class MyThread
    {

        public string name { get; set; }

        public IState state { get; set; }

        public void changeState()
        {
            state = state.Execute(this);
        }
    }
}
