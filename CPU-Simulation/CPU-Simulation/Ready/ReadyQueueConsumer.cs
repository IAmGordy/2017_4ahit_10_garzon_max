﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class ReadyQueueConsumer
    {
        ReadyQueue readyQueue = ReadyQueue.GetInstance();
        

        public void Consume()
        {
            while (true)
            {
                if(readyQueue.Count > 0)
                {
                    MyThread myth = (MyThread)readyQueue.Dequeue();
                    myth.changeState();
                }
                
            }
        }
    }
}
