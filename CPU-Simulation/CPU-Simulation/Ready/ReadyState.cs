﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class ReadyState : IState
    {
        static Random r = new Random();
        int number;
        public IState Execute(MyThread myth)
        {
            Console.WriteLine(myth.name + ".OldState: " + myth.state.ToString());
            number = r.Next(1, 100);
            if (number < 20)
            {
                Console.WriteLine(myth.name + ".NewState: Finished");
                return new FinishedState();
            }
            if (number >= 20 && number < 70)
            {
                Console.WriteLine(myth.name + ".NewState: Runnning");
                ReadyQueue.GetInstance().Enqueue(myth);
                return new RunningState();
            }
               
            else
            {
                Console.WriteLine(myth.name + ".NewState: Blocked");
                BlockQueue.GetInstance().Enqueue(myth);
                return new BlockedState();
            }
        }
    }
}
