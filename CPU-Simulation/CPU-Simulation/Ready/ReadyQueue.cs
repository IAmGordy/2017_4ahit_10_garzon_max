﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPU_Simulation
{
    class ReadyQueue : Queue
    {
        public static ReadyQueue readyqueue;
        
        public static ReadyQueue GetInstance()
        {
            return (readyqueue == null) ? readyqueue = new ReadyQueue() : readyqueue;
        }
    }
}
