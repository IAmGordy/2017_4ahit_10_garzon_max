﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CPU_Simulation
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadyQueue readyq = ReadyQueue.GetInstance();
            RunningQueue runningq = RunningQueue.GetInstance();
            BlockQueue blockq = BlockQueue.GetInstance();

            RunningQueueConsumer runningConsumer = new RunningQueueConsumer();
            ReadyQueueConsumer readyConsumer = new ReadyQueueConsumer();
            BlockQueueConsumer blockConsumer = new BlockQueueConsumer();

            for (int i = 0; i < 10; i++)
            {
                readyq.Enqueue(new MyThread() { name = "MyThready" + i, state = new ReadyState() });
                runningq.Enqueue(new MyThread() { name = "MyThready" + i, state = new RunningState() });
                blockq.Enqueue(new MyThread() { name = "MyThready" + i, state = new BlockedState() });
            }


            Thread thread1 = new Thread(blockConsumer.Consume);
            Thread thread2 = new Thread(readyConsumer.Consume);
            Thread thread3 = new Thread(runningConsumer.Consume);

            thread1.Start();
            thread2.Start();
            thread3.Start();
            Console.ReadKey();

        }
    }
}
