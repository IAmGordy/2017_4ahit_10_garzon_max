﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Minesweeper_v1
{
    class Bomb:IObject
    {
        ImageBrush icon;
        int id;
        int points;
        public void execute()
        {
            //EndGame();
        }

        public ImageBrush Geticon(ImageBrush icon)
        {
            this.icon = icon;
            return icon;
        }

        public int Getid(int id)
        {
            this.id = id;
            return id;
        }

        public int GetPoints(int points)
        {
            this.points = points;
            return points;
        }
    }
}
