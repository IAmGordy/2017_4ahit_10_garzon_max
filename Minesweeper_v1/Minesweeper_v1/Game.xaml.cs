﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Minesweeper_v1
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        private int button_id = 1;
        private int pos_x = 35;
        private int pos_y = 30;
        private int bombs_count = 10;
        private bool gamerunning = false;
        private Dictionary<int, IObject> GameList = new Dictionary<int, IObject>();
        private List<Button> GameField = new List<Button>();
        private List<int> Bombplaces_in_game = new List<int>();
        Check c = new Check();
        ImageBrush bomb_pic;
        Score s = new Score();
        bool[] lockScore = new bool[100];

        public Game()
        {
            InitializeComponent();
            DataContext = s;
            CreateField();
        }

        private void CreateField()
        {
            Width = 530;
            Height = 530;
            SetScore();
            for (int i = 1; i < 101; i++)
            {
                gridGame.Margin = new Thickness(0, 0, 0, 0); // setzt das Margin des Grid zurück

                Button b = new Button(); //Button wird generiert
                b.Name = "b_" + button_id.ToString();
                b.Margin = new Thickness(pos_x, pos_y, 0, 0);
                b.HorizontalAlignment = HorizontalAlignment.Left;
                b.VerticalAlignment = VerticalAlignment.Top;
                b.Width = 40;
                b.Height = 40;
                b.Content = null;
                b.Background = Brushes.White;
                b.Click += new RoutedEventHandler(b_Click); //Event mit check

                pos_x = pos_x + 45;
                if ((button_id == 10) ||
                    (button_id == 20) ||
                    (button_id == 30) ||
                    (button_id == 40) ||
                    (button_id == 50) ||
                    (button_id == 60) ||
                    (button_id == 70) ||
                    (button_id == 80) ||
                    (button_id == 90))
                {
                    pos_x = pos_x - 450;
                    pos_y = pos_y + 45;
                }
                gridGame.Children.Add(b);
                GameField.Add(b);
                button_id++;
            }
        }

        private void SetScore()
        {
            for (int setter = 0; setter < 100; setter++)
            {
                lockScore[setter] = false;
            }
        }

        private void b_Click(object sender, RoutedEventArgs e)
        {
            int temp_button_id = 0;
            if (gamerunning == false)
            {
                StartGame();
            }
            gamerunning = true;

            Button tempb = sender as Button;

            ButtonKlick(ConvertID(tempb.Name, temp_button_id));
        }

        public void StartGame()
        {
            CheckGameType();
            SetObjects();
        }
        public void EndGame()
        { }
        private void SetObjects()
        {
            Random rnd_bombs = new Random();

            for (int i = 0; i < bombs_count; i++)
            {
                IObject b = new Bomb();
                int help;
                help = rnd_bombs.Next(1, 100);
                while (GameList.ContainsKey(help) == true)
                {
                    help = rnd_bombs.Next(1, 100);
                }
                GameList.Add(help, b);
                Bombplaces_in_game.Add(help);
                MessageBox.Show(help.ToString());     //just for testing purposes
                b.Geticon(bomb_pic);
                bomb_pic = new ImageBrush { ImageSource = new BitmapImage(new Uri("bomb.jpg", UriKind.Relative)) };
            }            
        }

        private void ButtonKlick(int i)//int i = Button der geklickt wird;
        {

            
            //Button Klick
            if (c.CheckIfBomb(i, GameList))//wenn Bombe ->
            {
                ChangeButtonContentBomb(i);
            }
            else
            {
                ChangeButtonContentNumberField(i, c.CheckNumberField(i, GameList));
                if (c.CheckNumberField(i, GameList) == 0)
                {
                    ChangeButtonContentEmptyField(i);
                }
                if (lockScore[i] == false)
                {
                    s.myScore += 1;
                    lockScore[i] = true;
                }
                if (c.CheckIfMaxScore(s.myScore, MainWindow.gamemodehard) == true)
                {
                    GameWon();
                }


            }

            
            

            //IObject io = new Bomb();
            //int number = 0;
            //if (GameList.TryGetValue(i, out io) == true) {
            //    ChangeButtonContentBomb(i); }// Bombe auf i, restliche if sinnlos -> break
            //if (GameList.TryGetValue(i + 1, out io) == true) { number = number + 1; }//rechts daneben
            //if (GameList.TryGetValue(i - 1, out io) == true) { number = number + 1; }//links daneben
            //if (GameList.TryGetValue(i + 10, out io) == true) { number = number + 1; }//darunter
            //if (GameList.TryGetValue(i - 10, out io) == true) { number = number + 1; }//darüber
            //if (GameList.TryGetValue(i - 11, out io) == true) { number = number + 1; }//schräg links darüber
            //if (GameList.TryGetValue(i - 9, out io) == true) { number = number + 1; }//schräg rechts darüber
            //if (GameList.TryGetValue(i + 9, out io) == true) { number = number + 1; }//schräg links darunter
            //if (GameList.TryGetValue(i + 11, out io) == true) { number = number + 1; }//schräg rechts darunter
            //if (number == 0)
            //{
            //    ChangeButtonContentEmptyField(i);
            //}
            //else
            //{
            //    ChangeButtonContentNumberField(i, number);
            //}
            //b.Content = number
            //MessageBox.Show(number.ToString());
        }
        private void CheckGameType()
        {
            if (MainWindow.gamemodehard == false)
            {
                bombs_count = 10;
                MessageBox.Show(bombs_count + " Bombs have been planted!");
            }
            else
            if (MainWindow.gamemodehard == true)
            {
                bombs_count = 20;
                MessageBox.Show(bombs_count + " Bombs have been planted!");
            }
        }

        private void GameWon()
        {
            if (gamerunning == true)
            {
                gamerunning = false;
                RevealBombs();
                MessageBox.Show("All bombs have been defused!");
                this.Close();
            }
            
        }

        private void ChangeButtonContentEmptyField(int button_id)
        {
            Button b_temp = GameField.Find(b => b.Name == ConvertID(button_id));
            b_temp.Content = 0;
        }

        private void ChangeButtonContentNumberField(int button_id, int number)
        {
            Button b_temp = GameField.Find(b => b.Name == ConvertID(button_id));
            b_temp.Content = number;
        }

        private void ChangeButtonContentBomb(int button_id)
        {
            Button b_temp = GameField.Find(b => b.Name == ConvertID(button_id));
            if (b_temp.Background == Brushes.White)
            {
                b_temp.Background = new ImageBrush { ImageSource = new BitmapImage(new Uri("bomb.jpg", UriKind.Relative)) };
            }
            if (gamerunning == true)
            {
                gamerunning = false;
                RevealBombs();
                GameOver();
            }
           
        }

        private int ConvertID(string string_to_split, int button_id)//Konvertiert ButtonName in seine IntegerZahl (Position)
        {
            string[] name_to_split;

            name_to_split = string_to_split.Split('_');
            button_id = Convert.ToInt32(name_to_split[1]);

            return button_id;
        }
        private string ConvertID(int button_id)//Konvertiert button_id in Button.Name(string)
        {
            string temp_button_name = "b_" + button_id.ToString();
            return temp_button_name;
        }

        private void GameOver()
        {
            MessageBox.Show("Game over my friend!");
            this.Close();
        }

        private void RevealBombs()
        {
            for (int i = 0; i < Bombplaces_in_game.Count; i++)
            {
                ChangeButtonContentBomb(Bombplaces_in_game.ElementAt(i));
            }
        }
    }
}
