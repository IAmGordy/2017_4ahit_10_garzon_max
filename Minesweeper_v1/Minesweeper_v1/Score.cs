﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Minesweeper_v1
{
    class Score:INotifyPropertyChanged
    {

        private int _score;
        public int myScore
        {
            get { return _score; }
            set
            {
                _score = value;
                PropertyChanged(this, new PropertyChangedEventArgs("myScore"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
