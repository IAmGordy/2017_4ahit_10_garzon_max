﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Minesweeper_v1
{
    interface IObject
    {
        int Getid(int id);

        ImageBrush Geticon(ImageBrush icon);

        int GetPoints(int points);

        void execute();
    }
}
