﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Minesweeper_v1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Game a = new Game();
        private bool gamerunning = false;
        public static bool gamemodehard = false; //false -> easy mode -- true -> hard mode

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_EasyMode_Click(object sender, RoutedEventArgs e)
        {
            //Button easy mode clicked
            if (gamerunning == false)
            {
                gamerunning = true;
                gamemodehard = false;
                a.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Active Game running!");
            }
        }

        private void btn_HardMode_Click(object sender, RoutedEventArgs e)
        {
            //Button hard mode clicked
            if (gamerunning == false)
            {
                gamerunning = true;
                gamemodehard = true;
                a.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Active Game running!");
            }
        }
    }
}
