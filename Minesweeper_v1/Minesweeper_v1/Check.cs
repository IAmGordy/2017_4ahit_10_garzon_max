﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minesweeper_v1
{
    class Check
    {
        IObject io = new Bomb();

        public bool CheckIfBomb(int field_id, Dictionary<int, IObject> GameList)
        {
            
            if (GameList.TryGetValue(field_id, out io) == true)
            {
                return true;
            }
            else
                return false;
        }

        public int CheckNumberField(int field_id, Dictionary<int, IObject> GameList)
        {
            int number = 0;
            if (GameList.TryGetValue(field_id + 1, out io) == true) { number = number + 1; }//rechts daneben
            if (GameList.TryGetValue(field_id - 1, out io) == true) { number = number + 1; }//links daneben
            if (GameList.TryGetValue(field_id + 10, out io) == true) { number = number + 1; }//darunter
            if (GameList.TryGetValue(field_id - 10, out io) == true) { number = number + 1; }//darüber
            if (GameList.TryGetValue(field_id - 11, out io) == true) { number = number + 1; }//schräg links darüber
            if (GameList.TryGetValue(field_id - 9, out io) == true) { number = number + 1; }//schräg rechts darüber
            if (GameList.TryGetValue(field_id + 9, out io) == true) { number = number + 1; }//schräg links darunter
            if (GameList.TryGetValue(field_id + 11, out io) == true) { number = number + 1; }//schräg rechts darunter
            return number;
        }

        public bool CheckIfMaxScore(int score, bool gamemodehard)
        {
            if (gamemodehard == false)
            {
                if (score == 90)
                {
                    return true;
                }
            }
            if (gamemodehard == true)
            {
                if (score == 80)
                {
                    return true;
                }
            }
            return false;
        }

        //public int CheckWhatItIs(int id, int[] array)
        //{
        //    int value; //Bei 9 = Bombe, 0 = Empty, 1 > x < 9 = Number

        //    if (CheckIfBomb(id, array) == true)
        //    {
        //        return value = 9;
        //    }
        //    if (CheckIfEmpty(id, array) == true)
        //    {

        //        return value = 0;
        //    }

        //    return value = CheckSurroundedBombs(id, array);


        //}

        //private bool CheckIfBomb(int id, int[] array)
        //{
        //    foreach (int i in array)
        //    {
        //        if (id == array[i])
        //        {
        //            return true;
        //        }
        //    }
        //    return false;

        //}
        //private bool CheckIfEmpty(int id, int[] array)
        //{
        //    foreach (int i in array)
        //    {
        //        if (id == array[i])
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        //private bool CheckIfNumber(int id, int[] array)
        //{
        //    foreach (int i in array)
        //    {
        //        if (id == array[i])
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        //private int CheckSurroundedBombs(int id, int[] array)
        //{
        //    int counter = 0;
        //    //id = 26
        //    id = id - 11;   //checkt 15
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id++;           //checkt 16
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id++;           //checkt 17
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id += 8;        //checkt 25
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id += 2;          //checkt 27
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id += 8;        //checkt 35
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id++;           //checkt 36
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    id++;           //check 37
        //    if (CheckIfBomb(id, array) == true && id > 0)
        //    {
        //        counter++;
        //    }
        //    return counter;
        //}
    }
}
