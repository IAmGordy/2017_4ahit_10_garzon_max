﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    interface Account
    {
        double getBalance();
        bool isOverdraftAvailable();
        void credit(double anz);

    }
}
