﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    public class AccountAdapter:AAccount
    {
        OffshoreAccount offshoreAccount;
        public AccountAdapter(OffshoreAccount offshoreAccount) : base(offshoreAccount.getOffshoreBalance())
        {
            this.offshoreAccount = offshoreAccount;
        }

        public double getBalance()
        {
            double taxrate = offshoreAccount.getTaxRate();
            double balance = offshoreAccount.getOffshoreBalance();

            double taxbalance = taxrate * balance;
            double balanceaftertax = balance - taxbalance;
            return balanceaftertax;
        }
    }
}
