﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    class StandardAccount : AAccount
    {
        public StandardAccount(double size) : base(size)
        {
            setOverdraftAvailable(false);
        }
    }
}
