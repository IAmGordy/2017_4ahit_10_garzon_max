﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    class PlatinumAccount : AAccount
    {
        public PlatinumAccount(double balance) : base(balance)
        {
            setOverdraftAvailable(true);
        }
    }
}
