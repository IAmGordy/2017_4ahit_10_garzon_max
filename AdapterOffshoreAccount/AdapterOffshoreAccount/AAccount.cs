﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    public class AAccount : Account
    {
        double balance;
        bool isOverdraftAvailable2;

        public AAccount(double size)
        {
            this.balance = size;
        }

        public double getBalance()
        {
            return balance;
        }

        public void setOverdraftAvailable(bool isOverdraftAvailable)
        {
            this.isOverdraftAvailable2 = isOverdraftAvailable;
        }

        public bool isOverdraftAvailable()
        {
            return isOverdraftAvailable2;
        }

        public void credit(double credit)
        {
            balance += credit;
        }

        public String toString()
        {
            return " Balance=" + getBalance()
                    + " Overdraft: " + isOverdraftAvailable();
        }

    }
}
