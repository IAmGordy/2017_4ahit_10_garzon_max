﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    class Program
    {
        static void Main(string[] args)
        {
            StandardAccount sa = new StandardAccount(2000);
            Console.WriteLine("Account Balance: " + sa.getBalance() + "Overdraftable: "+ sa.isOverdraftAvailable());

            
            AccountAdapter adapter = new AccountAdapter(new OffshoreAccount(2000));
            Console.WriteLine("Account Balance: " + adapter.getBalance() + "Overdraftable: " + sa.isOverdraftAvailable());

            Console.ReadKey();
        }
    }
}
