﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdapterOffshoreAccount
{
    public class OffshoreAccount
    {
        double balance;
        static double TAX_RATE;

        public OffshoreAccount(double balance)
        {
            this.balance = balance;
        }

        public double getTaxRate()
        {
            return TAX_RATE;
        }
        public double getOffshoreBalance()
        {
            return balance;
        }

        public void debit(double debit)
        {
            if(balance>=debit)
            {
                balance -= debit;
            }
        }

        public void credit(double credit)
        {
            balance += credit;
        }
    }
}
