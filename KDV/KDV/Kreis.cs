﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KDV
{
    class Kreis:FlyObj
    {
        public Kreis(MainWindow main, Shape shape) : base(main)
        {
            this.main = main;
            this.shape = shape;

            Style button = new Style(typeof(Shape));
            button.Setters.Add(new EventSetter(Shape.MouseLeftButtonDownEvent, new MouseButtonEventHandler(buttonDown)));

            shape.Style = button;
        }

        void buttonDown(object o, EventArgs e)
        {
            GetPoints();
        }
    }
}
