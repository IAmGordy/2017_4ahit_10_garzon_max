﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace KDV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Kreis k;
        Viereck v;
        Dreieck d;
        public MainWindow()
        {
            InitializeComponent();
            k = new Kreis(this, Ball);
            v = new Viereck(this, Viereck);
            d = new Dreieck(this, Dreieck);
            comboBox_kreis.Items.Add("green");
            comboBox_kreis.Items.Add("blue");
            comboBox_kreis.Items.Add("red");

            comboBox_viereck.Items.Add("green");
            comboBox_viereck.Items.Add("blue");
            comboBox_viereck.Items.Add("red");

            comboBox_dreieck.Items.Add("green");
            comboBox_dreieck.Items.Add("blue");
            comboBox_dreieck.Items.Add("red");
        }

        private void button_kreis_Click(object sender, RoutedEventArgs e)
        {
            k.StartStop();           
        }

        private void button_viereck_Click(object sender, RoutedEventArgs e)
        {
            v.StartStop();
        }

        private void button_dreieck_Click(object sender, RoutedEventArgs e)
        {
            d.StartStop();
        }

        private void comboBox_kreis_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            k.SetColour(Convert.ToString(comboBox_kreis.SelectedItem), Ball);
        }

        private void comboBox_viereck_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            v.SetColour(Convert.ToString(comboBox_viereck.SelectedItem), Viereck);
        }

        private void comboBox_dreieck_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            d.SetColour(Convert.ToString(comboBox_dreieck.SelectedItem), Dreieck);
        }


    }
}
