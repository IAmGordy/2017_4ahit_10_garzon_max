﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace KDV
{
    abstract class FlyObj
    {
        DispatcherTimer timer;


        int score = 0;
        public Shape shape;
        public MainWindow main;

        public FlyObj(MainWindow main)
        {
            this.main = main;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += RunPhysics;
        }

        public void StartStop()
        {
            timer.IsEnabled = !timer.IsEnabled;
            // timer.Start();

        }

        public void SetColour(string colour, Shape e)
        {
            if (colour == "green")
                e.Fill = Brushes.Green;
            if (colour == "blue")
                e.Fill = Brushes.Blue;
            if (colour == "red")
                e.Fill = Brushes.Red;
        }

        private void RunPhysics(object o, EventArgs e)
        {
            CheckWhatItIs(shape);
        }

        //Ändern auf wenn der button gedrückt war ? jakob hats mit ner liste wo einfach nur wenn man auf start/stop drückt die geaddet 
        // oder entfernt werden
        bool ballup = true;
        bool ballright = true;
        bool viereckup = true;
        bool viereckright = true;
        bool dreieckup = true;
        bool dreieckright = true;

        private void CheckWhatItIs(Shape e2)
        {

            if (main.kreis_Start.IsChecked.Value)
            {
                e2 = main.Ball;
                Physics(e2);
            }

            if (main.viereck_Start.IsChecked.Value)
            {
                e2 = main.Viereck;
                Physics(e2);
            }

            if (main.dreieck_Start.IsChecked.Value)
            {
                e2 = main.Dreieck;
                Physics(e2);
            }

        }


        public void Physics(Shape e)
        {
            double speed = 3.0;

            //if (main.checkBox_kreis.IsChecked.Value)
            //{
            //    speed = 10.0;
            //}

            MoveLR(speed, e);
            MoveUD(speed, e);

        }
            bool down = true;
            bool right = true;
        public void MoveLR(double speed, Shape e)
        {
       
            double x = Canvas.GetLeft(e);


            if (right)
            {
                x += speed;
            }
            else
            {
                x -= speed;
            }

            if (x + e.Width > main.TheCanvas.ActualWidth)
            {
                right = false;
                x = main.TheCanvas.ActualWidth - e.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0.0)
            {
                right = true;
                x = 0.0;
                System.Media.SystemSounds.Beep.Play();
            }
            Canvas.SetLeft(e, x);
        }

        //public void MoveLR(double speed, Polygon p)
        //{
        //    double x = Canvas.GetLeft(p);

        //    if (GoingRight)
        //    {
        //        x += speed;
        //    }
        //    else
        //    {
        //        x -= speed;
        //    }

        //    if (x + p.Width > TheCanvas.ActualWidth)
        //    {
        //        GoingRight = false;
        //        x = TheCanvas.ActualWidth - p.Width;
        //        System.Media.SystemSounds.Asterisk.Play();
        //    }
        //    else if (x < 0.0)
        //    {
        //        GoingRight = true;
        //        x = 0.0;
        //        System.Media.SystemSounds.Beep.Play();
        //    }
        //    Canvas.SetLeft(p, x);
        //}

        //public void MoveLR(double speed, Rectangle r)
        //{
        //    double x = Canvas.GetLeft(r);

        //    if (GoingRight)
        //    {
        //        x += speed;
        //    }
        //    else
        //    {
        //        x -= speed;
        //    }

        //    if (x + r.Width > TheCanvas.ActualWidth)
        //    {
        //        GoingRight = false;
        //        x = TheCanvas.ActualWidth - r.Width;
        //        System.Media.SystemSounds.Asterisk.Play();
        //    }
        //    else if (x < 0.0)
        //    {
        //        GoingRight = true;
        //        x = 0.0;
        //        System.Media.SystemSounds.Beep.Play();
        //    }
        //    Canvas.SetLeft(r, x);
        //}

        public void MoveUD(double speed, Shape e)
        {

            double y = Canvas.GetTop(e);

            if (down)
            {
                y += speed;
            }
            else
            {
                y -= speed;
            }

            if (y + e.Height > main.TheCanvas.ActualHeight)
            {
                down = false;
                y = main.TheCanvas.ActualHeight - e.Height;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (y < 0.0)
            {
                down = true;
                y = 0.0;
                System.Media.SystemSounds.Hand.Play();
            }
            Canvas.SetTop(e, y);
        }

        //public void MoveUD(double speed, Polygon p)
        //{
        //    double y = Canvas.GetTop(p);

        //    if (GoingDown)
        //    {
        //        y += speed;
        //    }
        //    else
        //    {
        //        y -= speed;
        //    }

        //    if (y + p.Height > TheCanvas.ActualHeight)
        //    {
        //        GoingDown = false;
        //        y = TheCanvas.ActualHeight - p.Height;
        //        System.Media.SystemSounds.Asterisk.Play();
        //    }
        //    else if (y < 0.0)
        //    {
        //        GoingDown = true;
        //        y = 0.0;
        //        System.Media.SystemSounds.Hand.Play();
        //    }
        //    Canvas.SetTop(p, y);
        //}
        //public void MoveUD(double speed, Rectangle r)
        //{
        //    double y = Canvas.GetTop(r);

        //    if (GoingDown)
        //    {
        //        y += speed;
        //    }
        //    else
        //    {
        //        y -= speed;
        //    }

        //    if (y + r.Height > TheCanvas.ActualHeight)
        //    {
        //        GoingDown = false;
        //        y = TheCanvas.ActualHeight - r.Height;
        //        System.Media.SystemSounds.Asterisk.Play();
        //    }
        //    else if (y < 0.0)
        //    {
        //        GoingDown = true;
        //        y = 0.0;
        //        System.Media.SystemSounds.Hand.Play();
        //    }
        //    Canvas.SetTop(r, y);
        //}
        public void GetPoints()
        {
            if (timer.IsEnabled)
            {
                score++;
                main.lb_counter.Content = score;
            }
        }
    }
}

